default['gitlab-elk']['logstash']['heap_size'] = '8g'
default['gitlab-elk']['kibana']['port'] = '5601'
default['gitlab-elk']['kibana']['listen'] = '0.0.0.0'
default['gitlab-elk']['kibana']['fqdn'] = node['fqdn']
default['gitlab-elk']['kibana']['proxy_url'] = 'http://127.0.0.1:5601/'
default['gitlab-elk']['kibana']['cookie'] = nil
default['gitlab-elk']['kibana']['ssl_certificate'] = 'set in secrets'
default['gitlab-elk']['kibana']['ssl_key'] = 'set in secrets'
default['gitlab-elk']['kibana']['nginx_template'] = 'nginx-site-kibana.erb'
default['gitlab-elk']['kibana']['enable_auth'] = false
default['gitlab-elk']['kibana']['auth_user'] = nil
default['gitlab-elk']['kibana']['auth_pass'] = nil
default['gitlab-elk']['elastic']['cleanup_index'] = 'logstash'
default['gitlab-elk']['elastic']['host'] = '127.0.0.1'
default['gitlab-elk']['elastic']['port'] = '9200'
default['gitlab-elk']['elastic']['proto'] = 'http'
default['gitlab-elk']['kibana']['version'] = '5.4.0'
default['gitlab-elk']['logstash']['manage_template'] = true
default['gitlab-elk']['logstash']['version'] = '1:5.2.2-1'
default['gitlab-elk']['logstash']['pipeline_batch_size'] = '125'
default['gitlab-elk']['logstash']['pipeline_batch_delay'] = '5'
default['gitlab-elk']['logstash']['pipeline_output_workers'] = '1'
default['gitlab-elk']['logstash']['pipeline_unsafe_shutdown'] = false
default['gitlab-elk']['logstash']['pipeline_workers'] = '8'
# default['gitlab-elk']['logstash']['es_hosts'] = [ "127.0.0.1:9200", "127.0.0.2:9200" ]
default['gitlab-elk']['kibana']['enabled'] = false
default['gitlab-elk']['logstash']['enabled'] = false
# x-pack-settings
default['gitlab-elk']['kibana']['plugin-x-pack'] = false
default['gitlab-elk']['logstash']['plugin-x-pack'] = false

default['gitlab-elk']['secrets']['backend'] = 'chef_vault'
default['gitlab-elk']['secrets']['path'] = 'gitlab-elk'
default['gitlab-elk']['secrets']['key'] = '_default'

# pusbsub
default['gitlab-elk']['pubsubbeat']['home_dir'] = '/opt/pubsubbeat'
default['gitlab-elk']['pubsubbeat']['progs_dir'] = "#{node['gitlab-elk']['pubsubbeat']['home_dir']}/progs"
default['gitlab-elk']['pubsubbeat']['conf_file'] = "#{node['gitlab-elk']['pubsubbeat']['home_dir']}/pubsubbeat.yml"
default['gitlab-elk']['pubsubbeat']['field_file'] = "#{node['gitlab-elk']['pubsubbeat']['home_dir']}/fields.yml"
default['gitlab-elk']['pubsubbeat']['log_dir'] = "/var/log/pubsubbeat"
default['gitlab-elk']['pubsubbeat']['extra_opts'] = ""
default['gitlab-elk']['pubsubbeat']['project'] = "gitlab-production"
default['gitlab-elk']['pubsubbeat']['json_disable_indexes'] = []
default['gitlab-elk']['pubsubbeat']['num_workers'] = 8
default['gitlab-elk']['pubsubbeat']['num_queue_events'] = 2048
default['gitlab-elk']['pubsubbeat']['bulk_max_size'] = 4096

default['gitlab-elk']['pubsubbeat']['conf'] = {
  "pubsubbeat" => {
    "project_id" => node['gitlab-elk']['pubsubbeat']['project'],
    "topic" => node['hostname'],
    "subscription.name" => "#{node['hostname']}-sub",
    "subscription.retention_duration" => "168h",
    "json.enabled" => true,
    "json.add_error_key" => true
  },
  "cloud.id" => "secret",
  "cloud.auth" => "secret",
  "http.enabled" => true,
  "output.elasticsearch" => {
    "index" => "%{[beat.name]}-%{+yyyy.MM.dd}",
    "enabled" => true,
    "worker" => node['gitlab-elk']['pubsubbeat']['num_workers'],
    "bulk_max_size" => node['gitlab-elk']['pubsubbeat']['bulk_max_size']
  },
  "setup.template.name" => "%{[beat.name]}-%{[beat.version]}",
  "setup.template.pattern" => "%{[beat.name]}-%{[beat.version]}-*",
  "queue" => {
    "mem" => {
      "events" => node['gitlab-elk']['pubsubbeat']['num_queue_events']
    }
  }
}

default['gitlab-elk']['pubsubbeat']['fields'] = [{
  fields: [
    { required: true, type: 'text', name: 'message' },
    { required: false, type: 'object', name: 'json' },
    { required: true, type: 'text', name: 'message_id' },
    { required: true, type: 'date', name: 'publish_time', format: 'date' },
    { required: false, type: 'object', name: 'attributes' }
  ],
  key: 'pubsubbeat',
  title: 'pubsubbeat'
}]

# pubsub artifact
default['gitlab-elk']['pubsubbeat']['version'] = '1.1.0'
default['gitlab-elk']['pubsubbeat']['url'] = "https://github.com/GoogleCloudPlatform/pubsubbeat/releases/download/#{node['gitlab-elk']['pubsubbeat']['version']}/pubsubbeat-linux-amd64.tar.gz"
default['gitlab-elk']['pubsubbeat']['sha256sum'] = "947b140e0f750b2192caec9582c7922fd236cdc6e8227569eaadc5b5f3d89576"

# pubsubbeat user / group
default['gitlab-elk']['pubsubbeat']['user'] = "root"
default['gitlab-elk']['pubsubbeat']['group'] = "root"
