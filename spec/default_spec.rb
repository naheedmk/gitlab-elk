require_relative "spec_helper"
require 'chef-vault'

describe "gitlab-elk::default" do
  shared_examples 'configuring elk' do
    it "installs logstash" do
      expect(chef_run).to install_package('logstash').with(version: '1:5.2.2-1')
    end

    it "installs kibana" do
      expect(chef_run).to install_package('kibana').with(version: '5.4.0')
    end

    it 'creates the ssl files' do
      expect(chef_run).to create_file("/etc/ssl/kibana.#{kibana_fqdn}.crt").with(
        content: ssl_certificate
      )
      expect(chef_run).to create_file("/etc/ssl/kibana.#{kibana_fqdn}.key").with(
        content: ssl_key
      )
    end
  end

  let(:kibana_fqdn) { 'my.kibana.host' }
  let(:elk_properties) do
    {
      'logstash' => { 'enabled' => true },
      'kibana' => { 'fqdn' => kibana_fqdn, 'enabled' => true },
    }
  end
  let(:ssl_key) { 'my-key\n' }
  let(:ssl_certificate) { 'my-certificate\n' }
  let(:secrets) do
    {
      'gitlab-elk' => {
        'kibana' => {
          'ssl_key' => ssl_key,
          'ssl_certificate' => ssl_certificate
        }
      }
    }
  end

  context 'secrets in Chef vault' do
    let(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
        node.normal['gitlab-elk'] = elk_properties

        node.normal['gitlab-elk']['chef_vault'] = 'gitlab-elk'
      end.converge(described_recipe)
    end

    before do
      expect(::ChefVault::Item).to receive(:load).with('gitlab-elk', '_default')
        .and_return(secrets)
    end

    it_behaves_like 'configuring elk'
  end

  context 'secrets in gkms' do
    let(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
        node.normal['gitlab-elk'] = elk_properties

        node.normal['gitlab-elk']['secrets'] = {
          'backend' => 'gkms',
          'path' => 'some-path',
          'key' => 'some-key'
        }
      end.converge(described_recipe)
    end

    before do
      expect_any_instance_of(Chef::Recipe).to receive(:get_secrets)
        .with('gkms', 'some-path', 'some-key').and_return(secrets)
    end

    it_behaves_like 'configuring elk'
  end
end
